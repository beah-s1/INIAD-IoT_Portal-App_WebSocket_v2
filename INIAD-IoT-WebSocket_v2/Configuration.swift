//
//  Configuration.swift
//  INIAD-IoT-WebSocket_v2
//
//  Created by Kentaro on 2019/10/02.
//  Copyright © 2019 Kentaro. All rights reserved.
//

import Foundation
import UIKit

class AppConfiguration{
    private var dict:NSDictionary!
    
    init(){
        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        do{
            self.dict = NSDictionary.init(contentsOf: URL.init(string: path!)!)
        }catch{
            assert(false, "COULD NOT FIND CONFOGURATION.PLIST FILE")
        }
    }
    
    func getValue(forKey:String) -> String{
        return String(describing: dict[forKey])
    }
}

extension UIView {
    func parentViewController() -> UIViewController? {
        var parentResponder: UIResponder? = self
        while true {
            guard let nextResponder = parentResponder?.next else { return nil }
            if let viewController = nextResponder as? UIViewController {
                return viewController
            }
            parentResponder = nextResponder
        }
    }
}
