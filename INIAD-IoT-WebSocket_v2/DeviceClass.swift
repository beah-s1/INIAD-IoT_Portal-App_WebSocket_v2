//
//  DeviceClass.swift
//  INIAD-IoT-WebSocket_v2
//
//  Created by Kentaro on 2019/10/02.
//  Copyright © 2019 Kentaro. All rights reserved.
//

import Foundation
import UIKit
import Starscream
import SwiftyJSON

class Doorlock:UIView{
    var apiKey = ""
    var roomName = ""
    var deviceId = ""
    var ucode = ""
    var socket:WebSocketClient!
    
    @IBOutlet weak var locked: UISwitch!
    @IBOutlet weak var timeSpecifyUnlockField: UITextField!
    @IBOutlet weak var unlockUntilTimeText: UILabel!
    
    func initialize(){
        //機器情報のSubscribe
        socket.write(string: """
            [32,\(self.deviceId),{},"get.devices.doorlocks.\(self.ucode)"]
            """)
    }
    
    func setLockStatus(parameter: JSON){
        //ユーザー以外により状態が変更されるとき（INIAD Portal/REST API/機器の直接操作/ICカードリーダー）
        if let until = parameter["until"].int{
            let untilTimeObject = Date.init(timeIntervalSince1970: TimeInterval(until))
            let formatter = DateFormatter()
            formatter.dateFormat = "HH時mm分ss秒まで解錠"
            
            self.unlockUntilTimeText.text = formatter.string(from: untilTimeObject)
            self.unlockUntilTimeText.alpha = 1.0
        }else{
            self.unlockUntilTimeText.text = ""
            self.unlockUntilTimeText.alpha = 0.0
        }
        
        let status = parameter["locked"].boolValue
        self.locked.setOn(status, animated: true)
    }
    
    @IBAction func switchStatusChanged(_ sender: UISwitch) {
        //ユーザーの操作により状態が変更されるとき
        var command = ""
        
        //時間指定解錠（指定秒数後まで解錠）
        if self.timeSpecifyUnlockField.text == ""{
            command = """
            [48,0,{},"set.devices",[{"locked":\(String(describing: sender.isOn))},"doorlocks.\(self.ucode)","\(self.apiKey)"]]
            """
        }else{
            if Int(self.timeSpecifyUnlockField.text!)! > 600{
                command = """
                [48,0,{},"set.devices",[{"locked":\(String(describing: sender.isOn))},"doorlocks.\(self.ucode)","\(self.apiKey)"]]
                """
                timeSpecifyUnlockField.text = ""
                timeSpecifyUnlockField.endEditing(true)
                
                let alert = UIAlertController(title: "Error", message: "601秒以上の解錠は実行できません\n通常の解錠を実行します", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                
                if let parentViewController = self.parentViewController() as? RoomControlViewController{
                    parentViewController.present(alert, animated: true, completion: nil)
                }
                
            }else{
                command = """
                [48,0,{},"set.devices",[{"locked":\(String(describing: sender.isOn)),"timeout":\(self.timeSpecifyUnlockField.text!)},"doorlocks.\(self.ucode)","\(self.apiKey)"]]
                """
                timeSpecifyUnlockField.text = ""
                timeSpecifyUnlockField.endEditing(true)
                
            }
            
        }
        
        
        socket.write(string: command)
    }
}

class Aircon:UIView{
    var apiKey = ""
    var roomName = ""
    var deviceId = ""
    var ucode = ""
    var socket:WebSocketClient!
    
    @IBOutlet weak var deviceNameText: UILabel!
    var availableStatus = [Dictionary<String,String>]()
    var availableModes = [Dictionary<String,String>]()
    var availableWinds = [Dictionary<String,String>]()
    var availableTemperature = Dictionary<String,Int>()
    
    @IBOutlet var statusSwitch: UISwitch!
    @IBOutlet var modeSwitch: UISegmentedControl!
    @IBOutlet var temperatureBar: UISlider!
    @IBOutlet var windSwitch: UISegmentedControl!
    @IBOutlet weak var temperatureText: UILabel!
    
    func initialize(){
        //機器情報のSubscribeとUIの初期化
        socket.write(string: """
            [32,\(self.deviceId),{},"get.devices.aircons.\(self.ucode)"]
            """)
        
        //動作モード
        if self.availableModes.count != 0{
            modeSwitch.removeAllSegments()
            for i in 0...availableModes.count-1{
                modeSwitch.insertSegment(withTitle: availableModes[i]["text_ja"], at: modeSwitch.numberOfSegments, animated: true)
            }
            
        }else{
            modeSwitch.removeAllSegments()
            modeSwitch.insertSegment(withTitle: "なし", at: 0, animated: true)
            modeSwitch.isEnabled = false
        }
        //風量
        if self.availableWinds.count != 0{
            windSwitch.removeAllSegments()
            for i in 0...availableWinds.count-1{
                windSwitch.insertSegment(withTitle: availableWinds[i]["text_ja"], at: windSwitch.numberOfSegments, animated: true)
            }
        }else{
            windSwitch.removeAllSegments()
            windSwitch.insertSegment(withTitle: "なし", at: 0, animated: true)
            windSwitch.isEnabled = false
        }
        
        //温度
        if self.availableTemperature.count != 0{
            self.temperatureBar.maximumValue = Float(self.availableTemperature["maximum"]!)
            self.temperatureBar.minimumValue = Float(self.availableTemperature["minimum"]!)
        }else{
            self.temperatureBar.isEnabled = false
        }
    }
    
    
    func setAirconStatus(parameter: JSON){
        //print(parameter)
        
        //運転
        self.statusSwitch.setOn(parameter["on"].boolValue, animated: true)
        
        //動作モード
        if self.availableModes.count != 0{
            for i in 0...self.availableModes.count-1{
                if self.availableModes[i]["code"] != parameter["mode"].stringValue{
                    continue
                }
                
                self.modeSwitch.selectedSegmentIndex = i
                break
            }
        }
        
        //風量
        if self.availableWinds.count != 0{
            for i in 0...self.availableWinds.count-1{
                if self.availableWinds[i]["code"] != parameter["wind"].stringValue{
                    continue
                }
                
                self.windSwitch.selectedSegmentIndex = i
                break
            }
        }
        
        if let temperature = parameter["temperature"].int{
            self.temperatureBar.setValue(Float(temperature), animated: true)
            
            self.temperatureText.text = "\(temperature)℃"
        }else{
            self.temperatureText.text = "取得不可"
            self.temperatureText.isEnabled = false
        }
    }
    
    @IBAction func statusSwitchChanged(_ sender: UISwitch) {
        let command = """
            [48,0,{},"set.devices",[{"on":\(String(describing: sender.isOn))},"aircons.\(self.ucode)","\(self.apiKey)"]]
        """
        
        socket.write(string: command)
    }
    
    @IBAction func modeSwitchChanged(_ sender: UISegmentedControl) {
        let command = """
            [48,0,{},"set.devices",[{"mode":"\(self.availableModes[sender.selectedSegmentIndex]["code"]!)"},"aircons.\(self.ucode)","\(self.apiKey)"]]
        """
        socket.write(string: command)
    }
    
    @IBAction func temperatureBarChangeEnded(_ sender: UISlider) {
        let value = sender.value.rounded()
        self.temperatureBar.setValue(value, animated: true)
        let command = """
            [48,0,{},"set.devices",[{"temperature":\(Int(value))},"aircons.\(self.ucode)","\(self.apiKey)"]]
        """
        
        socket.write(string: command)
    }
    
    @IBAction func windSwitchChanged(_ sender: UISegmentedControl) {
        let command = """
            [48,0,{},"set.devices",[{"wind":"\(self.availableWinds[sender.selectedSegmentIndex]["code"]!)"},"aircons.\(self.ucode)","\(self.apiKey)"]]
        """
        
        socket.write(string: command)
    }
    
    @IBAction func temperatureBarChanged(_ sender: UISlider) {
        
        self.temperatureText.text = "\(Int(sender.value.rounded()))℃"
    }
    
    
}

class Light:UIView{
    var apiKey = ""
    var roomName = ""
    var deviceId = ""
    var ucode = ""
    var socket:WebSocketClient!
    
    var availableModes = [Dictionary<String,String>]()
    @IBOutlet var modeSwitch: UISegmentedControl!
    
    func initialize(){
        //機器情報のSubscribeとUIの初期化
        socket.write(string: """
            [32,\(self.deviceId),{},"get.devices.lights.\(self.ucode)"]
            """)
        modeSwitch.removeAllSegments()
        for i in 0...availableModes.count-1{
            modeSwitch.insertSegment(withTitle: availableModes[i]["text_ja"], at: modeSwitch.numberOfSegments, animated: true)
        }
    }
    
    func setLightStatus(parameter:JSON){
        //ユーザー以外により状態が変更されるとき（INIAD Portal/REST API/機器の直接操作）
        for i in 0...self.availableModes.count-1{
            if availableModes[i]["code"] != parameter["on"].stringValue{
                continue
            }
            
            modeSwitch.selectedSegmentIndex = i
            break
        }
    }
    
    @IBAction func modeSwitchChanged(_ sender: UISegmentedControl) {
        //ユーザーの操作により状態が変更されるとき
        let command = """
        [48,0,{},"set.devices",[{"on":\(availableModes[sender.selectedSegmentIndex]["code"]!)},"lights.\(self.ucode)","\(self.apiKey)"]]
        """
        
        socket.write(string: command)
    }
}

class Sensor:UIView{
    
}

class Wattmeter:UIView{
    
}
