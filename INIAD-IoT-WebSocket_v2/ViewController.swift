//
//  ViewController.swift
//  INIAD-IoT-WebSocket_v2
//
//  Created by Kentaro on 2019/10/01.
//  Copyright © 2019 Kentaro. All rights reserved.
//

import UIKit
import Starscream
import SwiftyJSON
import KeychainAccess

class RoomControlViewController: UIViewController, WebSocketDelegate {
    
    let apiKey = "YOUR_API_KEY" //APIキーはINIAD Portalからぶっこ抜く
    let roomName = "2416"
    @IBOutlet weak var roomNameLabel: UILabel!
    @IBOutlet weak var deviceDisplay: UIScrollView!
    let socket = WebSocket.init(url: URL(string: "wss://API_URL")!) //API URLは自分でキャプチャして確認してください
    var currentHeight = 0.0
    
    var doorlockDevices = [Doorlock]()
    var lightDevices = [Light]()
    var airconDevices = [Aircon]()
    var sensorDevices = [Sensor]()
    var wattmeterDevices = [Wattmeter]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.socket.delegate = self
        self.socket.connect()
    }

    func websocketDidConnect(socket: WebSocketClient) {
        //print("connected")
        socket.write(string: """
            [1,"javascript.\(self.apiKey)"]
            """)
    }
    
    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        
    }
    
    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        let messageJsonObject = JSON.init(parseJSON: text)
        print(messageJsonObject)
        
        switch messageJsonObject[0].int{
        case 2:
            //print("hello")
            self.initializeDevice()
        case 33:
            for layer in deviceDisplay.subviews{
                switch layer{
                case let doorlockLayer as Doorlock:
                    if doorlockLayer.deviceId != messageJsonObject[1].stringValue{
                        continue
                    }
                    
                    doorlockLayer.deviceId = messageJsonObject[2].stringValue
                    return
                case let lightLayer as Light:
                    if lightLayer.deviceId != messageJsonObject[1].stringValue{
                        continue
                    }
                    
                    lightLayer.deviceId = messageJsonObject[2].stringValue
                    return
                case let airconLayer as Aircon:
                    if airconLayer.deviceId != messageJsonObject[1].stringValue{
                        continue
                    }
                    
                    airconLayer.deviceId = messageJsonObject[2].stringValue
                    return
                default:break
                }
            }
        case 36:
            for layer in deviceDisplay.subviews{
                switch layer{
                case let doorlockLayer as Doorlock:
                    if doorlockLayer.deviceId != messageJsonObject[1].stringValue{
                        continue
                    }
                    
                    doorlockLayer.setLockStatus(parameter: messageJsonObject[4][0])
                    return
                case let lightLayer as Light:
                    if lightLayer.deviceId != messageJsonObject[1].stringValue{
                        continue
                    }
                    
                    lightLayer.setLightStatus(parameter: messageJsonObject[4][0])
                    return
                case let airconLayer as Aircon:
                    if airconLayer.deviceId != messageJsonObject[1].stringValue{
                        continue
                    }
                    
                    airconLayer.setAirconStatus(parameter: messageJsonObject[4][0])
                    return
                default:break
                }
            }
            break
        default:break
        }
    }
    
    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
        
    }
    
    func initializeDevice(){
        let path = Bundle.main.path(forResource: "RoomDevice", ofType: "json")!
        guard let roomDeviceListJsonString = try? String(contentsOfFile: path) else{
            assert(false, "COULD NOT FIND ROOM DEVICE LIST JSON FILE")
        }
        let roomDeviceListJsonObject = JSON.init(parseJSON: roomDeviceListJsonString)
        //print(roomDeviceListJsonObject)
        
        self.roomNameLabel.text = roomDeviceListJsonObject[self.roomName]["room_name"].stringValue
        
        for i in roomDeviceListJsonObject[self.roomName]["doorlocks"]{
            guard let device = Bundle.main.loadNibNamed("Doorlock", owner: self, options: nil)!.first as? Doorlock else{
                return
            }
            //let device = Doorlock.init()
            device.apiKey = self.apiKey
            device.deviceId = i.1["device_num"].stringValue
            device.roomName = self.roomName
            device.ucode = i.1["ucode"].stringValue
            device.socket = self.socket
            device.backgroundColor = .systemBackground
            device.frame = CGRect.init(x: 0.0, y: currentHeight, width: Double(self.view.frame.maxX), height: 200.0)
            self.currentHeight += 200.0
            
            device.initialize()
            
            self.deviceDisplay.addSubview(device)
        }
        
        for i in roomDeviceListJsonObject[self.roomName]["lights"]{
            guard let device = Bundle.main.loadNibNamed("Light", owner: self, options: nil)!.first as? Light else{
                return
            }
            
            device.apiKey = self.apiKey
            device.deviceId = i.1["device_num"].stringValue
            device.roomName = self.roomName
            device.ucode = i.1["ucode"].stringValue
            device.socket = self.socket
            device.backgroundColor = .systemBackground
            device.frame = CGRect.init(x: 0.0, y: currentHeight, width: Double(self.view.frame.maxX), height: 180.0)
            self.currentHeight += 180.0
            
            for mode in i.1["available_status"]{
                device.availableModes.append(["code":mode.1["code"].stringValue,"text_ja":mode.1["text_ja"].stringValue])
            }
            
            device.initialize()
            
            self.deviceDisplay.addSubview(device)
        }
        
        for i in roomDeviceListJsonObject[self.roomName]["aircons"]{
            guard let device = Bundle.main.loadNibNamed("Aircon", owner: self, options: nil)!.first as? Aircon else{
                return
            }
            
            device.apiKey = self.apiKey
            device.deviceId = i.1["device_num"].stringValue
            device.roomName = self.roomName
            device.ucode = i.1["ucode"].stringValue
            device.socket = self.socket
            device.backgroundColor = .systemBackground
            device.frame = CGRect.init(x: 0.0, y: currentHeight, width: Double(self.view.frame.maxX), height: 470.0)
            self.currentHeight += 470.0
            
            for mode in i.1["available_mode"]{
                device.availableModes.append(["code":mode.1["code"].stringValue,"text_ja":mode.1["text_ja"].stringValue])
            }
            
            for mode in i.1["available_wind"]{
                device.availableWinds.append(["code":mode.1["code"].stringValue,"text_ja":mode.1["text_ja"].stringValue])
            }
            
            if let availableTemperature = i.1["available_temperature"].dictionary{
                device.availableTemperature["maximum"] = availableTemperature["maximum"]?.intValue
                device.availableTemperature["minimum"] = availableTemperature["minimum"]?.intValue
            }
            
            if let deviceType = i.1["device_type"].string{
                device.deviceNameText.text = deviceType
            }
            
            device.initialize()
            
            self.deviceDisplay.addSubview(device)
        }
        
        self.deviceDisplay.contentSize.height = CGFloat(self.currentHeight)
        //print(self.doorlockDevices)
    }
}

